#include <stdint.h>
#include <stdlib.h>
#include "bmp.h"

struct bmp_info * new_bmp() {
    struct bmp_info * bmp =  malloc(sizeof(struct bmp_info));
    bmp->img = malloc(sizeof(struct image));
    return bmp;
}

struct image * rotate_image(const struct image * source) {
    struct image * result = malloc(sizeof(struct image));
    result->width = source->height;
    result->height = source->width;
    result->data = malloc(sizeof(struct pixel) * result->width * result->height);
    for (int i = 0; i < source->width; i++) {
        for (int j = 0; j < source->height; j++) {
            result->data[(i + 1) * source->height - (j + 1)] = source->data[j * source->width + i];
        }
    }
    return result;
}

void rotate(struct bmp_info * source_bmp){
    uint32_t width = source_bmp->bmi->bi_width;
    source_bmp->bmi->bi_width = source_bmp->bmi->bi_height;
    source_bmp->bmi->bi_height = width;
    source_bmp->img = rotate_image(source_bmp->img);
}