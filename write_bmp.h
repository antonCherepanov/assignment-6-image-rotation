#ifndef _WRITE_BMP_H
#define _WRITE_BMP_H

enum write_status {
    WRITE_OK,
    WRITE_ERROR
};

enum write_status to_bmp(FILE * out, const struct bmp_info * bmp);

#endif //_WRITE_BMP_H
