gcc -c -pedantic-errors -Wall -o main.o main.c -lm
gcc -c -pedantic-errors -Wall -o bmp.o bmp.c -lm
gcc -c -pedantic-errors -Wall -o read_bmp.o read_bmp.c -lm
gcc -c -pedantic-errors -Wall -o write_bmp.o write_bmp.c -lm
gcc -o main main.o bmp.o read_bmp.o write_bmp.o