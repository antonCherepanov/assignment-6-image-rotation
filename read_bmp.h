#ifndef _READ_BMP_H
#define _READ_BMP_H

enum read_status{
	READ_OK,
    READ_INVALID_BIT_MAP_FILE_HEADER,
    READ_INVALID_BIT_COUNT
};

enum read_status from_bmp(FILE * in, struct bmp_info *bmp);

#endif //_READ_BMP_H
