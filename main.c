#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "bmp.h"
#include "read_bmp.h"
#include "write_bmp.h"

int main() {
    struct bmp_info * bmp = new_bmp();
    char src_filename[50];
    fgets(src_filename, 50, stdin);
    strtok(src_filename, "\n");
    char dist_filename[15];
    FILE * src_file = fopen(src_filename, "r");
    sprintf(dist_filename, "(rotated).bmp");
    FILE * dist_file = fopen(dist_filename, "w");
    from_bmp(src_file, bmp);
    rotate(bmp);
    to_bmp(dist_file, bmp);
}